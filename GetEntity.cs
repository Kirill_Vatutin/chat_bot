﻿using System.Collections.Generic;
using System.Xml;

namespace Chat_bot_new
{
    static partial class GetEntity//в этой части инициализируем переменные
    {
        //листы, в которых хранятся варианты приветствий, на которые реаигрует пользователь

        //static GetEntity()
        //{
        //    XmlDocument xml = new XmlDocument();
        //    xml.Load(@"C:\Users\vatut\source\repos\Chat_bot_new\Chat_bot_new\Models\BotAnswer\HelloAnswerBot.xml");
        //    XmlElement element = xml.DocumentElement;
        //    foreach (XmlNode item in element)
        //    {
        //        if (item.Name == "answer")
        //        {
        //            HelloAnswer.Add(item.InnerText);

        //        }

        //    }
        //}
            static List<string> HelloAnswer = new List<string>
        {
            "привет",
            "здравствуй",
            "здравствуйте",
            "добрый день",
            "добрый вечер",
            "доброе утро",
            "доброй ночи"
        };



            static List<string> WhatIsYourName = new List<string>
        {
            "как тебя зовут?",
            "твое имя?",
            "ты Шарпик?"
        };

            static List<string> AnecdoteAnswer = new List<string>
        {
        "шутку дай",
        "анекдот"
        };

            static List<string> WhatTimeIsIt = new List<string>
        {
        "сколько времени?",
        "который час?"
        };


            static List<string> GoodByeAnswer = new List<string>
        {
        "пока",
        "до свидания"
        };

            /////////////////////////////////////////////

            static List<string> HelloBotAnswer = new List<string>
        {
            "привет, человек",
            "доброго времени суток",
            "рад вас видеть"
        };


            static List<string> Anekdote = new List<string>
        {
            "*тут шутка*",
            "русалка утонула"
        };

            static List<string> Aphorism = new List<string>
        {
            "век живи, век учись",
            "вабота не волк, работа это work, а walk это ходить",
            "афоризм"
        };

            static List<string> GoodByeBotAnswer = new List<string>
        {
            "пока,человек",
            "до свидания!"
        };

        }

        static partial class GetEntity //здесь мы реализуем методы
        {
            static public List<string> GetHelloAnswer()
            {
                return HelloAnswer;
            }

            static public List<string> GetWhatIsYourName()
            {
                return WhatIsYourName;
            }

            static public List<string> GetWhatTimeIsIt()
            {
                return WhatTimeIsIt;
            }
            static public List<string> GetGoodByeAnswer()
            {
                return GoodByeAnswer;
            }
            /////////////////////////////////////////
            ///
            static public List<string> GetHelloBotAnswer()
            {
                return HelloBotAnswer;
            }
            static public List<string> GetAnekdote()
            {
                return Anekdote;
            }
            static public List<string> GetAphorism()
            {
                return Aphorism;
            }
            static public List<string> GetGoodByeBotAnswer()
            {
                return GoodByeBotAnswer;
            }

            static public List<string> GetAnecdoteAnswer()
            {
                return AnecdoteAnswer;
            }



        }
    }
